<!-- Latest compiled and minified CSS -->
<?php
session_start();
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<form class="form-horizontal" action="" method="post">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="atik_turu">Atık Türü</label>
  <div class="col-md-4">
    <select id="atik_turu" name="atik_turu" onchange="aktifet();" class="form-control">
      <option value="Yanmis Yag">YANMIŞ YAĞ</option>
      <option value="Mavi Kapak">MAVİ KAPAK</option>
      <option value="Kagit">KAĞIT</option>
      <option value="Atik Pil">ATİK PİL</option>
      <option value="diger">Diğer</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="diger">Diğer secenek</label>  
  <div class="col-md-4">
  <input id="diger" name="diger"  type="text" placeholder="deger girin" class="form-control input-md">
  <span class="help-block">acilir listeden diğer değerini secip değer yazabilirisniz.</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="miktar">Miktar</label>  
  <div class="col-md-4">
  <input id="miktar" name="miktar" type="text" placeholder="Miktar Belirtiniz" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="aciklama">Aciklama</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="aciklama" name="aciklama"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-success">kaydet</button>
  </div>
</div>

</fieldset>
</form>
<script>
      document.getElementById("diger").disabled = true;

    function aktifet(){
        var a=document.getElementById("atik_turu").value;

        if(a=="diger"){
        document.getElementById("diger").disabled = false;
        }
        else{
            document.getElementById("diger").disabled = true;
        }

    }
  
</script>

<?php
if(isset($_POST['aciklama'])){
    include('baglanti.php');
    if($_POST['atik_turu']=="diger"){
        $atik_turu=$_POST['diger'];
    }
    else{   
         $atik_turu=$_POST['atik_turu'];
    }

    $miktar=$_POST['miktar'];
    $aciklama=$_POST['aciklama'];

    $query = $db->prepare("INSERT INTO malzemeler SET
adres = ?,
atik_turu = ?,
miktar = ?,
email = ?,
telefon = ?,
aciklama = ?");
$insert = $query->execute(array(
$_SESSION['adres'],$atik_turu,$miktar,$_SESSION['email'],$_SESSION['telefon'],$aciklama
));
if ( $insert ){
    $last_id = $db->lastInsertId();
    header("location:index.php");
}
else{
    echo "hata";
}
    
}
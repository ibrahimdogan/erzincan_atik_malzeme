<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<form class="form-horizontal" action="" method="post">
<fieldset>

<!-- Form Name -->
<legend>Kayıt ekranı</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adi">Adı</label>  
  <div class="col-md-4">
  <input id="adi" name="adi" type="text" placeholder="Adınızı giriniz..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="soyadi">Soyadınız</label>  
  <div class="col-md-4">
  <input id="soyadi" name="soyadi" type="text" placeholder="Soyadınızı giriniz..." class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Telefon</label>  
  <div class="col-md-4">
  <input id="email" name="telefon" type="text" placeholder="Email giriniz.." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="email" placeholder="Email giriniz.." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sifre">sifre</label>  
  <div class="col-md-4">
  <input id="sifre" name="sifre" type="text" placeholder="Şifre giriniz" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="adres">Adres</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="adres" name="adres"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button id="" name="" class="btn btn-success">Kaydet</button>
  </div>
</div>

</fieldset>
</form>

<?php
if(isset($_POST['email'])){
    include('baglanti.php');
    $adi=$_POST['adi'];
    $soyadi=$_POST['soyadi'];
    $email=$_POST['email'];
    $telefon=$_POST['telefon'];
    $sifre=$_POST['sifre'];
    $adres=$_POST['adres'];

    $query = $db->prepare("INSERT INTO uyeler SET
adi = ?,
soyadi = ?,
email = ?,
telefon = ?,
sifre = ?,
adres = ?");
$insert = $query->execute(array(
$adi,$soyadi,$email,$telefon,$sifre,$adres
));
if ( $insert ){
    $last_id = $db->lastInsertId();
    header("location:index.php");
}
    
}


?>